package com.android.metrotrackr;

import java.util.ArrayList;
import java.util.UUID;

import com.example.catchit_stl.model.Station;

import android.content.Context;

public class FooStations {
	private ArrayList<Station> mStations;

	private static FooStations sFooStations;
	private Context mAppContext;
	
	public static FooStations get(Context c){
		if(sFooStations == null){
			sFooStations = new FooStations(c.getApplicationContext());
		}
		return sFooStations;
	}

	private FooStations(Context appContext) {
		mAppContext = appContext;
		mStations = new ArrayList<Station>();
		for (int i = 0; i < 50; i++) {
			Station s = new Station();
			s.setName("Station #" + i);
			mStations.add(s);
		}
	}
	public ArrayList<Station> getStations(){
		return mStations;
	}
}
